import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      displayMsg: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
    };
    this.loadCityInfo = this.loadCityInfo.bind(this);
  }

  reverseString(str) {
      var newString = "";
      for (var i = str.length - 1; i >= 0; i--) { 
          newString += str[i];
      }
      return newString;
  }

  loadCityInfo() {
    console.log('here');
    const self = this;
    this.setState({ displayMsg: 'loading...' });
    axios.get('https://baconipsum.com/api/?type=meat-and-filler&paras=5&format=text')
    .then(function (response) {
      console.log(response);
      self.setState({ displayMsg: self.reverseString(response.data) });
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  render() {
    return (
      <div className="Main_div">
        <div className="row m-l-0 m-r-0">
          <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div className=" text-center">
              <div className="header_main_div" onClick={this.loadCityInfo}>Header</div>
            </div>
          </div>
        </div>

        <div className="center_main_div">
          <div className="row m-l-0 m-r-0" >
            <div className="left_column">
               {this.state.displayMsg}
            </div>
            <div className="right_column">Center Text</div>
          </div>
        </div>
      </div>

      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <h1 className="App-title">Welcome to se</h1>
      //   </header>
      //   <p className="App-intro">
      //     To get started, edit <code>src/App.js</code> and save to reload.
      //   </p>
      // </div>
    );
  }


}

var GetUrl 



export default App;
